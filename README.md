linux-bootstrap
===============

Project for setting up my linux machines. This project helps me to reinstall my 
linux machine or install a new machine from scratch. Feel free to open pull 
requests.

There might be other projects like this one but I started this someday as it 
fits for me. So maybe things are not nice or not the best linux way to do s.th.,
but they work for me.

contents
--------
- install.sh: installs some additional packages, defined in "10-additional-pkgs.txt"
- exports.sh: some additional shell variable exports
- profile: the shell profile, symlink this to $HOME/.profile
