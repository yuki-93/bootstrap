NAS Vorbereiten
###############

- Doku: https://wiki.ubuntuusers.de/Samba_Client_cifs/

- Eintrag in /etc/hosts:
`192.168.1.107 nas_home`

- Mount point anlegen
`sudo mkdir /media/nas_backup`

- Packages installieren
`sudo apt install cifs-utils` 

- Datei ~/.smbcredentials anlegen und `chmod 600` auf Datei, Inhalt:
`
username=foo
password=bar
`

- Eintrag in /etc/fstab
`//nas_home/backup /media/nas_backup cifs auto,credentials=/home/florian/.smbcredentials 0 0`

- Mount
`sudo mount /media/nas_backup`

Backups mit Borg
################

- Herunterladen und in excecutable dir verschieben (ggf. muss root wegen /media/ oeffnen koennen)

- Backup Repo initalizieren: borg init -e repokey /media/nas_backup

- Password merken, Key sichern borg export REPO PATH_KEYFILE

- Backup erstellen: borg create -v --stats REPO::NAME PATH_TO_BACKUP

- Backups auflisten: borg list

- Backups zurueckholen: `borg extract` oder `borg export-tar` 
