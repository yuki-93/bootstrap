#!/bin/bash

# require me in e.g. ~/.profile: source ./exports.sh
export VISUAL=`which vim`
export EDITOR=`which vim`

if [ -d "$HOME/bin" ] ; then
  export PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
  export PATH="$HOME/.local/bin:$PATH"
fi
